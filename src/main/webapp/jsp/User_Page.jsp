<%@page import="org.pnv.model.Account"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>IT SHARING</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="./assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="./assets/bootstrap/css/style2.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="./assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
	<%@include file="Header.jsp" %>
	<div class="container" style="margin-top:70px">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="margin-left:-20px;">
    			<div class="row">
    				<div class="well">
    					<div class="row text-center">
    						
    							<p><a href=""><img src="./assets/images/avatar/${anotherAccount.profile.avatar}" alt="" class="img-circle" width="100" height="100"></a></p>
    								
          							
          							
    					
    						
    						
	    					<p><a href="#"><h3>${anotherAccount.profile.name}</h3></a></p>
	    					
    					</div>
    					<div class="row">
    						
    							<p class="text-center">
    								<a data-toggle="modal" href='#modal-id'>Friend</a>
    								<div class="modal fade" id="modal-id">
    									<div class="modal-dialog">
    										<div class="modal-content">
    											<div class="modal-header">
    												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    												<h4 class="modal-title">Your friends</h4>
    											</div>
    											
    											<div class="modal-body">
    											<c:forEach var="friend" items="${anotherAccount.friendList}">
    												<div class="media">
    													<a class="pull-left" href="#">
    														<img class="media-object img-circle" src="./assets/images/avatar/${friend.avatar}" alt="Image" width="50" height="50">
    													</a>
    													
    													<div class="media-body">
    														<h5 class="media-heading"><a href="homeServlet?command=anotherUser$id=${friend.id_user }">${friend.name}</a></h5>
    														<small style="color:gray;">Friend</small>
    														<button type="button" class="btn btn-default pull-right">Hủy kết bạn</button>
    													</div>
    												</div>
    											</c:forEach>
    												
    												
    											</div>
    											<div class="modal-footer">
    												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    												<button type="button" class="btn btn-primary">Save changes</button>
    											</div>
    											
    										</div>
    									</div>
    								</div>
    							</p>
    							<p class="text-center"><strong>${anotherAccount.size }</strong></p>
    						
    						
    					</div>
    				</div>
    			</div>
    			
    			<div class="row">
    				<div class="well">
    					<p><strong>About </strong><small><a href="#">. Edit</a></small></p>
    					<div class="row">
    						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    							<ul class="my-group-list">
    								<li><span class="glyphicon glyphicon-sunglasses"></span></li>
    								<li><span class="glyphicon glyphicon-list-alt"></span></li>
    								<li><span class="glyphicon glyphicon-home"></span></li>
    								<li><span class="glyphicon glyphicon-phone"></span></li>
    								
    							</ul>
    						</div>
    						<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
    							<ul class="my-group-list">
    								<li>Tên: <a href="#">${anotherAccount.profile.name}</a></li>
    								<li>Làm việc tại: <a href="#">${anotherAccount.profile.job}</a></li>
    								<li>Đang sống tại: <a href="#">${anotherAccount.profile.city}</a></li>
    								<li>SĐT: <a href="#">${anotherAccount.profile.phone}</a></li>
    								
    							</ul>
    						</div>
    					</div>
    				</div>
    			</div>
    			

    			
    		</div>
    		<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="margin-left:10px;">
    			<div class="row">
    				<div class="panel panel-default">
    					
    				</div>
    			</div>
    			
    			<div id="new-post"></div>
    			<c:forEach var="article" items="${anotherAccount.articles }">
    				<div class="row">
    					<div class="panel panel-default">
    						<div class="panel-heading">
    							<div class="media">
    						<a class="pull-left" href="#">
    							<img class="media-object" src="./assets/images/avatar/${article.avatar}" alt="Image" width="50" height="auto">
    						</a>
    						<div class="media-body">
    							<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${article.idUser }">${article.nameOfUser}</a></h5>
    							<small><i>${article.dateCreate}</i></small>
    							<p>${article.content }</p>
    						</div>
    					</div>
    					<hr>
    					<ul class="list-inline">
    						<li><a id="${article.idPost }-like" onclick="like('${article.idPost}')"><span class="glyphicon glyphicon-thumbs-up"></span>Like</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-comment"></span> Bình Luận</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-link"></span> Chia Sẻ</a></li>
    					</ul>
    						</div>
    						<div class="panel-body">
    							<span class="glyphicon glyphicon-thumbs-up"></span><a href="#" id="${article.idPost }-display"> ${article.numberOfLike }</a>
    							<div id="${article.idPost }-newComment"></div>
								<c:forEach var="comment" items="${article.comments }">
    							<div class="media" >
    								<a class="pull-left" href="#">
    									<img class="media-object" width="50" height="auto" src="./assets/images/avatar/${comment.avatar}" alt="Image">
    								</a>
    								<div class="media-body">
    									<h5 class="media-heading"><a href="#">${comment.nameOfUser }</a> ${comment.content }</h5>
    									<p><a href="#">Thích</a> . <a href="#">Trả lời</a></p>
    									
    								</div>
    							</div>
    							<hr>
								</c:forEach>
									<img src="./assets/images/avatar/${account.profile.avatar}" class="img-thumbnail" width="45" alt="">
									<input type="text" id="${article.idPost }-input-comment" class="my-input" placeholder="Bình luận" maxlength="300">
									<button type="button" id="${article.idPost }-comment" onclick="newComment('${article.idPost}')" class="btn btn-primary">Bình luận</button>
									<input type="hidden" id="${article.idPost }-command-comment" name="command-comment" value="comment">
				
								
    							
    							
    						</div>
    					</div>

    					
    				
    			</div>
    			</c:forEach>
    			
    		</div>
    		<%@include file="Right_Content.jsp" %>
    		</div>
</body>
</html>