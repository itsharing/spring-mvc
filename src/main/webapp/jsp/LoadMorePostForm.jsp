<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
           <c:forEach var="post" items="${posts}">
           		<div class="row">
    					<div class="panel panel-default">
    						<div class="panel-heading">
    							<div class="media">
    						<a class="pull-left" href="#">
    							<img class="media-object" src="./assets/images/avatar/${post.avatar}" alt="Image" width="50" height="auto">
    						</a>
    						<div class="media-body">
    							<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${post.idUser }">${post.nameOfUser}</a></h5>
    							<small><i>${post.dateCreate}</i></small>
    							<p>${post.content }</p>
    						</div>
    					</div>
    					<hr>
    					<ul class="list-inline">
    						<li><a href="#"><span class="glyphicon glyphicon-thumbs-up"></span> Thích</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-comment"></span> Bình Luận</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-link"></span> Chia Sẻ</a></li>
    					</ul>
    						</div>
    						<div class="panel-body">
    							<a href="#"><span class="glyphicon glyphicon-thumbs-up"></span> Mỹ Linh và 11 người khác</a>
    							<div id="new-comment"></div>
								<c:forEach var="comment" items="${post.comments }">
									
    							<div class="media">
    								<a class="pull-left" href="#">
    									<img class="media-object" width="50" height="auto" src="./assets/images/avatar/${comment.avatar}" alt="Image">
    								</a>
    								<div class="media-body">
    									<h5 class="media-heading"><a href="#">${comment.nameOfUser }</a> ${comment.content }</h5>
    									<p><a href="#">Thích</a> . <a href="#">Trả lời</a></p>
    									<!-- <div class="media">
    										<a class="pull-left" href="#">
    											<img class="media-object" src="./assets/images/avatar/${comment.avatar}" alt="Image" width="50" height="auto">
    										</a>
    										<div class="media-body">
    											<h5 class="media-heading"><a href="#">Tên user</a> nội dung bình luận</h5>
    											<p><a href="#">Thích</a> . <a href="#">Trả lời</a>  . <a href=""><small>8 giờ</small></a></p>
    										</div>
    									</div> -->
    								</div>
    							</div>
    							<hr>
								</c:forEach>
									<img src="./assets/images/avatar/${account.profile.avatar}" class="img-thumbnail" width="45" alt="">
									<input type="text" id="input-comment" class="my-input" placeholder="Bình luận" maxlength="300">
									<button type="button" id="comment" class="btn btn-primary">Bình luận</button>
									<input type="hidden" id="command-comment" name="command-comment" value="comment">
									<input type="hidden" id="id-post" value="${post.idPost }">
								
    							
    							
    						</div>
    					</div>

    					
    				
    			</div>
           </c:forEach>
