package org.pnv.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;

import org.pnv.business.AccountBo;
import org.pnv.business.AccountBoImpl;
import org.pnv.model.Account;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

@Controller
public class PostController {
	
	private AccountBo accountBoImpl = new AccountBoImpl();
	
	@RequestMapping(value = "/likePost", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public void LikePost(@RequestParam("idPost") int id_post, HttpServletRequest request,HttpServletResponse response, ModelMap map) {
		HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        
        account = accountBoImpl.Like(account, id_post);
        session.removeAttribute("account");
        session.setAttribute("account", account);
        for(int i = 0; i < account.getArticles().size();i++){
            if(account.getArticles().get(i).getIdPost() == id_post){
                response.setContentType("text");
                try {
					response.getWriter().write(""+account.getArticles().get(i).getNumberOfLike());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
             break;
           }
        }
        
    }
	
	@RequestMapping(value = "/unlikePost", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public void UnLikePost(@RequestParam("idPost") int id_post,HttpServletRequest request,HttpServletResponse response, ModelMap map) {
		HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        account = accountBoImpl.UnLike(account, id_post);
        session.removeAttribute("account");
        session.setAttribute("account", account);
        for(int i = 0; i < account.getArticles().size();i++){
            if(account.getArticles().get(i).getIdPost() == id_post){
                response.setContentType("text");
                try {
					response.getWriter().write("" + account.getArticles().get(i).getNumberOfLike());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                break;
            }
        }
        
    }
	
	@RequestMapping(value = "/post", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public void Post( HttpServletRequest request,HttpServletResponse response, ModelMap map) {
		accountBoImpl = new AccountBoImpl();
		HttpSession session = request.getSession();
		Account account = (Account) session.getAttribute("account");
		String input = request.getParameter("input");
		account = accountBoImpl.Article(account, input);
		session.removeAttribute("account");
	    session.setAttribute("account", account);
	    request.setAttribute("newArticle", account.getArticles().get(account.getArticles().size()-1));
		final StringWriter buffer = new StringWriter();
        try {
			request.getRequestDispatcher("jsp/Post_Form.jsp").include(request, new HttpServletResponseWrapper(response) {
			    private PrintWriter writer = new PrintWriter(buffer);

			    @Override
			    public PrintWriter getWriter() throws IOException {
			        return writer;
			    }
			});
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        String html = buffer.toString();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("html", html);
		response.setContentType("application/json");  
		response.setCharacterEncoding("UTF-8"); 
		try {
			response.getWriter().write(new Gson().toJson(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
        
    }
	
	@RequestMapping(value = "/comment", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public void Comment( HttpServletRequest request,HttpServletResponse response, ModelMap map) {
		accountBoImpl = new AccountBoImpl();
		HttpSession session = request.getSession();
		Account account = (Account) session.getAttribute("account");
		String content = request.getParameter("input");
		int idPost = Integer.parseInt(request.getParameter("idPost"));
		account = accountBoImpl.addComment(account, content, idPost);
		session.removeAttribute("account");
	    session.setAttribute("account", account);
	    int noOfPost = 0;
	    for(int i = 0; i < account.getArticles().size(); i++){
	    	if(account.getArticles().get(i).getIdPost() == idPost){
	    		noOfPost = i;
	    		break;
	    	}
	    	
	    }
	    int noOfComment = account.getArticles().get(noOfPost).getComments().size() - 1;
	    request.setAttribute("newComment", account.getArticles().get(noOfPost).getComments().get(noOfComment));
		final StringWriter buffer = new StringWriter();
        try {
			request.getRequestDispatcher("jsp/Comment_Form.jsp").include(request, new HttpServletResponseWrapper(response) {
			    private PrintWriter writer = new PrintWriter(buffer);

			    @Override
			    public PrintWriter getWriter() throws IOException {
			        return writer;
			    }
			});
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        String html = buffer.toString();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("html", html);
		response.setContentType("application/json");  
		response.setCharacterEncoding("UTF-8"); 
		try {
			response.getWriter().write(new Gson().toJson(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
        
    }
}
