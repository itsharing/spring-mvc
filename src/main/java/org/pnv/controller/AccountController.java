package org.pnv.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.pnv.business.AccountBo;
import org.pnv.business.AccountBoImpl;
import org.pnv.model.Account;
import org.pnv.model.AccountRequest;
import org.pnv.model.SearchResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AccountController {
	
	
	private AccountBo accountBoImpl = new AccountBoImpl();
	
	
	
	@RequestMapping(value="/viewLogin", method = RequestMethod.GET,produces = "application/json; charset=utf-8")
	public String viewLoginPage(ModelMap map){
		return "login";
	}
	
	@RequestMapping(value="/login", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String Login(@RequestParam("form-username") String email, @RequestParam("form-password") String password, ModelMap map, HttpServletRequest request){
		int result = accountBoImpl.checkAccountExist(email, password);
		if (result == 1 ) {
			HttpSession session = request.getSession(false);
			session.setAttribute("account", accountBoImpl.getAccount(email, password));
			return "home_page";
		}else if(result == 0){
			map.addAttribute("email", email);
			map.addAttribute("password", password);
			return "verify_code";
		} else{
			map.addAttribute("error", "wrong user or password");
			return "register_new";
		}
	}
	
	@RequestMapping(value="/register", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String register(@RequestParam("email") String email,@RequestParam("password") String password,@RequestParam("name") String name,
			@RequestParam("gender") int gender,@RequestParam("dob") String dob,@RequestParam("job") String job,@RequestParam("city") String city,ModelMap map){
		AccountRequest accountRequest = new AccountRequest(email,password,name,dob,gender,job,city);
		boolean result = accountBoImpl.registerAccount(accountRequest);
		if(result == true){
			map.addAttribute("email", email);
			map.addAttribute("password", password);
			return "verify_code";
		}else{
			return "register_new";
		}
		
	}
	
	@RequestMapping(value="/verifyCode", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String VerifyCode(@RequestParam("email") String email,@RequestParam("password") String password, 
			@RequestParam("verifyCode") String verifyCode, ModelMap map,HttpServletRequest request){
		
		if(accountBoImpl.checkVerifyCode(verifyCode, email) == true){
			HttpSession session = request.getSession(false);
			session.setAttribute("account", accountBoImpl.getAccount(email, password));
			return "home_page";
		}else{
			map.addAttribute("email", email);
			map.addAttribute("password", password);
			map.addAttribute("error", "wrong verify code");
			return "verify_code";
		}
	}
	
	@RequestMapping(value = "/changeProfile", method = RequestMethod.GET,produces = "application/json; charset=utf-8")
    public String ViewChangeProfile(ModelMap map) {
        return "profile";
    }
	
	@RequestMapping(value="/changeProfile", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
	public String ChangeProfile(HttpServletRequest request, @RequestParam("name") String name, @RequestParam("gender") int gender, @RequestParam("dob") String dob,
			@RequestParam("job") String job, @RequestParam("city") String city, @RequestParam("phone") String phone, ModelMap map){
		HttpSession session = request.getSession();
		Account account = (Account) session.getAttribute("account");
		
		accountBoImpl = new AccountBoImpl();
		account = accountBoImpl.changeProfile(account,name,gender,dob,job,city,phone);
		session.removeAttribute("account");
		map.addAttribute("account", account);
		map.addAttribute("note", "Change profile successfully");
		return "profile";	
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    public String ChangePassword(ModelMap map, HttpServletRequest request, @RequestParam("oldPassword") String oldPassword,
    		@RequestParam("newPassword") String newPassword,@RequestParam("confirmPassword") String confirmPassword) {
		HttpSession session = request.getSession();
		Account account = (Account) session.getAttribute("account");
		boolean checkPassword = accountBoImpl.changePassword(account, oldPassword, newPassword, confirmPassword);
		if( !checkPassword){
			request.setAttribute("note", "Can not change your password, your password wrong");
			return "profile";
		}else{
			request.setAttribute("note", "Change password successfully");
			return "profile";
		}
    }
	
	@RequestMapping(value="/logout", method = RequestMethod.GET,produces = "application/json; charset=utf-8")
	public String Logout(HttpServletRequest request, ModelMap map){
		HttpSession session = request.getSession();
		session.invalidate();
		return "register_new";
	}
	
    @RequestMapping(value= "/changeImage", method = RequestMethod.POST)
    public String changeImage(@RequestParam("avatar") String avatar, ModelMap map, HttpServletRequest request){
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        accountBoImpl.changeProfilePicture(account, avatar);
        session.removeAttribute("account");
        session.setAttribute("account", account);
        return "home_page";
    }
    
    @RequestMapping(value= "/search", method = RequestMethod.POST)
    public String search(@RequestParam("data-search") String dataSearch, ModelMap map){
        List<SearchResult> listUser = accountBoImpl.searchUsers(dataSearch);        
        if( listUser.isEmpty() ){
            map.addAttribute("searchFail", "There is no result matches your search");
            return "info_searching_page";
        }else{
            map.addAttribute("searchSuccess", "There are " + listUser.size() + " results match your search");
            map.addAttribute("list", listUser);                
            return "info_searching_page";
        }    
    }
    
    @RequestMapping(value = "/addFriend", method = RequestMethod.POST)
    public String addFriend(@RequestParam("addAFriend") String addFriend, ModelMap map, HttpServletRequest request){
        HttpSession sesion = request.getSession();
        Account account =(Account) sesion.getAttribute("account");
        accountBoImpl.addFriend(account, addFriend);
        return "home_page";
    }
    
    @RequestMapping(value = "/agreeFriend", method = RequestMethod.POST)
    public String agreeFriend(@RequestParam("agreeAFriend") String agreeFriend, ModelMap map, HttpServletRequest request){
        HttpSession sesion = request.getSession();
        Account account =(Account) sesion.getAttribute("account");
        accountBoImpl.agreeFriend(account, agreeFriend);
        return "home_page";
    }
    
    @RequestMapping(value = "/deleteFriend", method = RequestMethod.POST)
    public String deleteFriend(@RequestParam("deleteAFriend") String deleteFriend, ModelMap map, HttpServletRequest request){
        HttpSession sesion = request.getSession();
        Account account =(Account) sesion.getAttribute("account");
        accountBoImpl.deleteFriend(account, deleteFriend);
        return "home_page";
    }

	
}