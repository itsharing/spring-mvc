package org.pnv.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET,produces = "application/json; charset=utf-8")
    public String viewWelcomePage(HttpServletRequest request, ModelMap map) {
		HttpSession session = request.getSession(true);
		if(session.getAttribute("account") != null){
			return "home_page";
		}else{
			return "register_new";
		}
        
    }
}
