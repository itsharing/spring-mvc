package org.pnv.model;

public class AccountRequest {
	private int id;
	private String email;
	private String password;
	private Profile profile;
	
	public AccountRequest() {
		super();
		profile = new Profile();
	}
	public AccountRequest(String email, String password, String name,String dob, int gender, String job, String city) {
		super();
		profile = new Profile();
		this.email = email;
		this.password = password;
		this.profile.setDob(dob);
		this.profile.setGender(gender);
		this.profile.setJob(job);
		this.profile.setCity(city);
		this.profile.setName(name); 
	}
	
	public AccountRequest(int id, String name,String dob, int gender, String job, String city,String phone) {
		super();
		profile = new Profile();
		this.id = id;
		this.profile.setDob(dob);
		this.profile.setGender(gender);
		this.profile.setJob(job);
		this.profile.setCity(city);
		this.profile.setName(name); 
		this.profile.setPhone(phone);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Profile getProfile() {
		return profile;
	}
	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	
	
	
	
}
