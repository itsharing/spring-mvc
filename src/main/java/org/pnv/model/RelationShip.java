package org.pnv.model;

public class RelationShip {
	private int status;
	private int id_friend;
	

	public RelationShip(int status, int id_friend) {
		super();
		this.status = status;
		this.id_friend = id_friend;
	}


	public RelationShip() {
		super();
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public int getId_friend() {
		return id_friend;
	}

	public void setId_friend(int id_friend) {
		this.id_friend = id_friend;
	}

	

	
}

