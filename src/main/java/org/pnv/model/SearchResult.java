package org.pnv.model;

public class SearchResult {
	private int id;	
	Profile profile;
			
	public SearchResult() {
		super();
	}

	public SearchResult(Profile profile) {
		super();
		this.profile = profile;
	}

	public SearchResult(int id,  Profile profile) {
		super();
		this.id = id;		
		this.profile = profile;
	}
		

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	
	
}
