package org.pnv.business;

import java.util.List;
import org.pnv.dao.AccountDaoImpl;
import org.pnv.dao.ArticalDaoImpl;
import org.pnv.dao.FriendDaoImpl;
import org.pnv.dao.ProfileDaoImpl;
import org.pnv.model.Account;
import org.pnv.model.AccountRequest;
import org.pnv.model.SearchResult;
import org.pnv.util.Utils;

public class AccountBoImpl implements AccountBo {
	
	ProfileDaoImpl profileDaoImpl;
	AccountDaoImpl accountDaoImpl;
	FriendDaoImpl friendDaoImpl;
	ArticalDaoImpl articleDaoImpl;

	public int checkAccountExist(String email, String password) { // Check
																	// account
																	// service ?
		accountDaoImpl = new AccountDaoImpl();
		return accountDaoImpl.selectDataToCheckLogin(email, password);
	}

	public boolean registerAccount(AccountRequest accountRequest) {
		accountDaoImpl = new AccountDaoImpl();
		String code = Utils.verifyEmailToActivedAccount(accountRequest.getEmail(),
				accountRequest.getProfile().getName());
		Account account = new Account(accountRequest);
		account.setVerifyCode(code);
		return accountDaoImpl.insertNewAccount(account);
	}

	public boolean checkVerifyCode(String verifyCode, String email) {
		accountDaoImpl = new AccountDaoImpl();
		if (verifyCode.equals(accountDaoImpl.selectCode(email))) {
			return accountDaoImpl.updateStatus(email);
		}
		return false;
	}

	public boolean isExistEmail(String email) {
		accountDaoImpl = new AccountDaoImpl();
		return accountDaoImpl.selectEmail(email);

	}

	public Account getAccount(String email, String password) {
		accountDaoImpl = new AccountDaoImpl();
		friendDaoImpl = new FriendDaoImpl();
		profileDaoImpl = new ProfileDaoImpl();
		Account account = new Account();
		account = accountDaoImpl.selectAccount(email, password);
		account = friendDaoImpl.selectFriendOfUser(account);
		account = friendDaoImpl.selectFriendsRequest(account);
		account = friendDaoImpl.selectStatusFriend(account);
		account = profileDaoImpl.selectProfile(account);
		account = friendDaoImpl.selectPeopleMayKnow(account);
		
		account = setArticlesForAccount(account);
		//account = accountDaoImpl.selectComment(account);
		return account;
	}
	
	public Account getAccount(int idUser) {
		accountDaoImpl = new AccountDaoImpl();
		friendDaoImpl = new FriendDaoImpl();
		profileDaoImpl = new ProfileDaoImpl();
		Account account = new Account();
		account = accountDaoImpl.selectAccount(idUser);
		account = friendDaoImpl.selectFriendOfUser(account);
		account = friendDaoImpl.selectFriendsRequest(account);
		account = friendDaoImpl.selectStatusFriend(account);
		account = profileDaoImpl.selectProfile(account);
		account = friendDaoImpl.selectPeopleMayKnow(account);		
		account = setArticlesForAccount(account);
		
		//account = accountDaoImpl.selectComment(account);
		return account;
	}

	public Account changeProfile(AccountRequest accountRequest) {
		profileDaoImpl = new ProfileDaoImpl();
		
		Account account = new Account(accountRequest);
		boolean check = profileDaoImpl.isUpdateInformationSuccessfull(account);
		if (check == true) {
			account = profileDaoImpl.selectProfile(account);
			return account;
		} else {
			return null;
		}
	}
	
	
	public Account changeProfile(Account account, String name, int gender, String dob, String job, String city, String phone) {
		profileDaoImpl = new ProfileDaoImpl();
		boolean check = profileDaoImpl.isUpdateInformationSuccessfull(account,  name,  gender,  dob,  job,  city,  phone);
		if (check == true) {
			account = profileDaoImpl.selectProfile(account);
			return account;
		} else {
			return null;
		}
	}

	
	public boolean changePassword(Account account, String oldPassword, String newPassword, String reenter_newPassword) {
		profileDaoImpl = new ProfileDaoImpl();
		if (newPassword.equals(reenter_newPassword)) {
			boolean check = profileDaoImpl.isUpdatePasswordSuccessful(account.getEmail(), oldPassword, newPassword);
			if (check == true) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	
	public Account changeProfilePicture(Account account, String ULR_Avatar) {
		profileDaoImpl = new ProfileDaoImpl();
		if (profileDaoImpl.isUpdateAvatarSuccessful(ULR_Avatar, account.getIdUser())) {
			account.getProfile().setAvatar(profileDaoImpl.selectAvatar(account.getIdUser()));
			return account;
		} else {
			return account;
		}
	}

	
	public Account setArticlesForAccount(Account account) {
		articleDaoImpl = new ArticalDaoImpl();
		return articleDaoImpl.selecetArticleWithArticle(account);
	}

	
	public Account postArticle(int idUser, String content) {
		articleDaoImpl = new ArticalDaoImpl();
		Account account = null;
		if(articleDaoImpl.insertArticalOfUser(idUser, content) == true){
			account = getAccount(idUser);
		}
		return account;
	}

	
	public List<SearchResult> searchUsers(String name) {
		profileDaoImpl = new ProfileDaoImpl();
		String data_search = Utils.normalizationDataSearch(name);
		List<SearchResult> listUser = profileDaoImpl.search(data_search);
		return listUser;
	}

	
	public boolean addFriend(Account account, String addFriend) {
		friendDaoImpl = new FriendDaoImpl();
		if(friendDaoImpl.addFriend(account, addFriend)){
			friendDaoImpl.selectFriendOfUser(account);
			friendDaoImpl.selectFriendsRequest(account);
			friendDaoImpl.selectStatusFriend(account);
			return true;
		}
		return false;
	}
	
	public boolean deleteFriend(Account account, String addFriend) {
		friendDaoImpl = new FriendDaoImpl();
		if(friendDaoImpl.deleteFriend(account, addFriend)){
			friendDaoImpl.selectFriendOfUser(account);
			friendDaoImpl.selectFriendsRequest(account);
			friendDaoImpl.selectStatusFriend(account);
			return true;
		}
		return false;
	}
	
	public boolean agreeFriend(Account account, String addFriend) {
		friendDaoImpl = new FriendDaoImpl();
		if(friendDaoImpl.agreeFriend(account, addFriend)){
			friendDaoImpl.selectFriendOfUser(account);
			friendDaoImpl.selectFriendsRequest(account);
			friendDaoImpl.selectStatusFriend(account);
			return true;
		}
		return false;
	}

	
	public Account Article(Account account, String postContent) {
        articleDaoImpl = new ArticalDaoImpl();
        return articleDaoImpl.insertArticalOfUser(account, postContent);

	}

	
	public Account addComment(Account account, String content, int idPost) {
		articleDaoImpl = new ArticalDaoImpl();
        return articleDaoImpl.insertComment(account, content,idPost);
	}
	
    public Account Like(Account account, int id_post) {       
    	articleDaoImpl = new ArticalDaoImpl();
        if(articleDaoImpl.insertLike(account.getIdUser(), id_post) == true){
            for(int i = 0; i < account.getArticles().size();i++){
                if(account.getArticles().get(i).getIdPost() == id_post){
                    account.getArticles().get(i).setNumberOfLike(articleDaoImpl.selectNumberOfLike(account.getIdUser(), id_post));
                }
            }
        }
        
        return account;

    }

    
    public Account UnLike(Account account, int id_post) {
    	articleDaoImpl = new ArticalDaoImpl();
        if(articleDaoImpl.deleteLike(account.getIdUser(), id_post)== true){
            for(int i = 0; i < account.getArticles().size();i++){
                if(account.getArticles().get(i).getIdPost() == id_post){
                    account.getArticles().get(i).setNumberOfLike(articleDaoImpl.selectNumberOfLike(account.getIdUser(), id_post));
                }
            }
        }
        return account;
    }

	
}
