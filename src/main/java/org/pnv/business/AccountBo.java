package org.pnv.business;

import java.util.List;

import org.pnv.model.Account;
import org.pnv.model.AccountRequest;
import org.pnv.model.SearchResult;

public interface AccountBo {

	int checkAccountExist(String email, String password);

	boolean registerAccount(AccountRequest accountRequest);
	
	boolean checkVerifyCode( String verifyCode ,String email);
	
	boolean isExistEmail(String email);
	
	Account getAccount(String email, String password);
	
	Account getAccount(int idUser);
	
	Account changeProfile(AccountRequest accountRequest);
	
	boolean changePassword(Account account, String oldPassword, String newPassword, String reenter_newPassword);
	
	Account changeProfilePicture(Account account, String ULR_Avatar);
	
	//Account postArticle(Account account);
	
	Account setArticlesForAccount(Account account);
	
	//Account arrangeArticleWithDateCreate(Account account);
	
	List<SearchResult> searchUsers(String name);

	Account changeProfile(Account account, String name, int gender, String dob, String job, String city, String phone);

	Account Article(Account account, String postContent);

	Account postArticle(int idUser, String content);

	boolean addFriend(Account account, String id_friend);

	boolean agreeFriend(Account account, String id_friend);

	boolean deleteFriend(Account account, String id_friend);
	
	Account addComment(Account account,String content,int idPost);
    Account Like(Account account, int id_post);
    Account UnLike(Account account, int id_post);

}

