package org.pnv.dao;



import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import org.pnv.model.Account;
import org.pnv.util.DBHelper;
import org.pnv.util.Utils;

public class AccountDaoImpl implements AccountDao {	
	
	public int selectDataToCheckLogin(String email, String password) {
		String sql = "SELECT isActive FROM account WHERE email = ? AND password = ?";
		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = -1;
		try {
			ps = cn.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, Utils.convertPasswordToMD5(password));
			rs = ps.executeQuery();
			if(rs.next()){
				result = rs.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		
		return result;
		
	}
	
	public boolean insertNewAccount(Account account) {
		Connection cn = DBHelper.returnConnection();
		CallableStatement ps = null;
		int result = 0;
		try {
			String sql = "CALL insert_account(?, ?, ?, ?, ?, ?, ?, ?,?)";
			ps = cn.prepareCall(sql);
			ps.setString(1, account.getEmail());
			ps.setString(2, Utils.convertPasswordToMD5(account.getPassword()));
			ps.setString(3, account.getVerifyCode());
			ps.setString(4, account.getProfile().getName());
			ps.setString(5, account.getProfile().getDob());
			ps.setInt(6, account.getProfile().getGender());
			ps.setString(7, account.getProfile().getCity());
			ps.setString(8, account.getProfile().getJob());
			ps.registerOutParameter(9, Types.INTEGER);
			
			ps.executeUpdate();
			result = ps.getInt(9);
		} catch (SQLException ex) {
			return false;
		}finally {
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		if(result == 0){
			return false;
		}else{
			return true;
		}
		
	}

	public boolean updateStatus(String email) {
		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		try {			
			String sql = "UPDATE account SET isActive = 1 WHERE email = ?";
			ps = cn.prepareStatement(sql);
			ps.setString(1, email);
			int result = ps.executeUpdate();
			if (result == -1) {
				return false;
			}else{
				return true;
			}
		} catch (SQLException ex) {
			
		}finally {
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		
		return false;

	}

	public String selectCode(String email) {
		String sql = "SELECT verify_code FROM account WHERE email = ?";

		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = null;
		try {
			ps = cn.prepareStatement(sql);

			ps.setString(1, email);
			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		
		return result;

	}

	
	public boolean selectEmail(String email) {
		String sql = "SELECT email FROM account WHERE email = ?";
		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = cn.prepareStatement(sql);

			ps.setString(1, email);
			rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			System.out.println("something went wrong!");
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return false;
	}	

	public Account selectAccount(String email, String password) {
		Account account = null; 
		String sql = "SELECT id_user,isActive,verify_code,date_create,date_update FROM account WHERE email = ? AND password = ?";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, Utils.convertPasswordToMD5(password));	
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				account = new Account();
				account.setIdUser(rs.getInt(1));
				account.setEmail(email);
				account.setPassword(Utils.convertPasswordToMD5(password));
				account.setIsActive(rs.getInt(2));
				account.setVerifyCode(rs.getString(3));
				account.setDateCreate(rs.getString(4));
				account.setDateUpdate(rs.getString(5));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}
	
	
	public Account selectAccount(int idUser) {
		Account account = null; 
		String sql = "SELECT email,password,isActive,verify_code,date_create,date_update FROM account WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, idUser);			
			rs = ps.executeQuery();			
			if(rs.next()){
				account = new Account();
				account.setIdUser(idUser);
				account.setEmail(rs.getString(1));
				account.setPassword(rs.getString(2));
				account.setIsActive(rs.getInt(3));
				account.setVerifyCode(rs.getString(4));
				account.setDateCreate(rs.getString(5));
				account.setDateUpdate(rs.getString(6));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}
	
//	public static void main(String[] args) {
//        Account account = new Account();
//        account.setIdUser(1);
//       AccountDaoImpl accountDaoImpl = new AccountDaoImpl();
//       accountDaoImpl.selectStatusFriend(account);
//               for(int o= 0 ; o <account.getRelationShip().size();o++ ){
//                     System.out.print(account.getRelationShip().get(o).getId_friend());
//                     System.out.print("+");
//                     System.out.print(account.getRelationShip().get(o).getStatus());
//                     System.out.print("  \n");
//              
//               }              
// }

	
}
