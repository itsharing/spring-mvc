package org.pnv.dao;

import org.pnv.model.Account;

public interface FriendDao {
	
	boolean deleteFriend(Account account, String id_friend);
	boolean agreeFriend(Account account, String id_friend);
	boolean addFriend(Account account, String id_friend);
	Account selectFriendsRequest(Account account);
	Account selectFriendOfUser(Account account);
	Account selectStatusFriend(Account account);
	Account selectPeopleMayKnow(Account account);
	
}
