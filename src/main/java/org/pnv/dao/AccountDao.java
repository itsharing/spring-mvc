package org.pnv.dao;

import org.pnv.model.Account;

public interface AccountDao {
	
	int selectDataToCheckLogin(String email, String password);
	boolean insertNewAccount(Account account);
	boolean updateStatus(String email);
	String selectCode(String email);
	boolean selectEmail(String email);	
	Account selectAccount(String email, String password);	
	Account selectAccount(int idUser);
		
}
