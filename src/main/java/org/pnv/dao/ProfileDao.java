package org.pnv.dao;

import java.util.List;

import org.pnv.model.Account;
import org.pnv.model.Profile;
import org.pnv.model.SearchResult;

public interface ProfileDao {
	
	Account selectProfile(Account account);
	boolean isUpdateInformationSuccessfull(Account account);
	boolean isUpdateAvatarSuccessful(String linkAvatar, int id);
	String selectAvatar(int id);	
	Profile selectOneProfile(int id_user);
	boolean isUpdatePasswordSuccessful(String email, String oldPassword, String newPassword);
	boolean isUpdateInformationSuccessfull(Account account, String name, int gender, String dob, String job,String city, String phone);
	List<SearchResult> search(String nameUserSearch);
}
