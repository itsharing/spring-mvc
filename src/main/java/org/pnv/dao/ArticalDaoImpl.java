package org.pnv.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import org.pnv.model.Account;
import org.pnv.model.Article;
import org.pnv.model.Comment;
import org.pnv.util.DBHelper;

public class ArticalDaoImpl implements ArticalDao{


    public Account insertArticalOfUser(Account account, String content) {
        Article article = new Article();
        String sql = "CALL insert_post(?,?,?,?,?)";
        Connection cn = DBHelper.returnConnection();
        CallableStatement ps = null;
        try{
            ps = cn.prepareCall(sql);
            ps.setInt(1, account.getIdUser() );
            ps.setString(2, content);
            ps.registerOutParameter(3, Types.INTEGER);
            ps.registerOutParameter(4, Types.VARCHAR);
            ps.registerOutParameter(5, Types.DATE);
            ps.executeUpdate();
            article.setIdPost(ps.getInt(3));
            article.setContent(ps.getString(4));
            article.setDateCreate(ps.getString(5));
            article.setIdUser(account.getIdUser());
            article.setNameOfUser(account.getProfile().getName());
            article.setAvatar(account.getProfile().getAvatar());
            account.getArticles().add(article);
            return account;
        }catch(Exception e){
            return account;
        }finally{
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
    }
    
    public Account selsectArticalOfUser(Account account) {
		String sql = "SELECT * FROM post WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		List<Article> articleList = new ArrayList<Article>(); 
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			ResultSet result = prepare.executeQuery();
			while(result.next()){
				Article article = new Article();
				article.setIdPost(result.getInt(1));
				article.setIdUser(result.getInt(2));
				article.setContent(result.getString(3));
				article.setDateCreate(result.getString(4));
				article.setDateUpdate(result.getString(5));
				articleList.add(article);
			}
			cn.close();
			account.setArticles(articleList);
		}catch(Exception e){
			System.out.println("Error function selsectArticalOfUser ");
		}
		return account;	
	}

	
	public boolean insertArticalOfUser(int id_user, String content) {
		String sql = "INSERT INTO  id_user = ?,content = ?,date_create = now() FROM post";
		Connection cn = DBHelper.returnConnection();
		try{
			PreparedStatement prepare = cn.prepareStatement(sql);
			String id = Integer.toString(id_user);
			prepare.setString(1, id );
			prepare.setString(2, content);
			int result = prepare.executeUpdate();
			if( result == 1){
				cn.close();
				return true;
			}else {
				cn.close();
				return false;
			}
		}catch(Exception e){
			System.out.println("Error function insertArticalOfUser ");
		}
		return false;
	}

	
	public boolean updateArticalOfUser(int id_user, int id_post, String content) {
		String sql = "UPDATE post SET  content = ? ,date_update = now() WHERE id_post = ? AND id_user = ?";
		Connection cn = DBHelper.returnConnection();
		try{
			PreparedStatement prepare = cn.prepareStatement(sql);
			String idUser  = Integer.toString(id_user);
			String idPost = Integer.toString(id_post);
			prepare.setString(1, content);
			prepare.setString(2, idPost);
			prepare.setString(3,  idUser );
			int result = prepare.executeUpdate();
			if( result == 1){
				cn.close();
				return true;
			}else {
				cn.close();
				return false;
			}
		}catch(Exception e){
			System.out.println("Error function updateArticalOfUser ");
		}
		return false;
	}

	
	public boolean deletetArticalOfUser(int id_post, int id_user) {
		String sql = "DELETE FROM post WHERE id_post = ? AND is_user = ?";
		Connection cn = DBHelper.returnConnection();
		try{
			PreparedStatement prepare = cn.prepareStatement(sql);
			String idUser  = Integer.toString(id_user);
			String idPost = Integer.toString(id_post);
			prepare.setString(1, idPost);
			prepare.setString(2,  idUser );
			int result = prepare.executeUpdate();
			if( result == 1){
				cn.close();
				return true;
			}else {
				cn.close();
				return false;
			}
		}catch(Exception e){
			System.out.println("Error function deletetArticalOfUser ");
		}
		return false;
		
	}
	
	public Account selecetArticleWithArticle(Account account) {
        //AccountDao accountDaoImpt = new AccountDaoImpl();
        ArticalDao articleDaoImpl = new ArticalDaoImpl();
        List<Article> articles = new ArrayList<Article>();
        Connection cn = DBHelper.returnConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String selections = String.valueOf(account.getIdUser());
        for (int i = 0; i < account.getFriendList().size(); i++) {
        	selections = selections + "," + account.getFriendList().get(i).getId_user();
        }
        
        String sql = "SELECT P.id_post, P.id_user, P.content, P.date_create, P.date_update, PF.name, PF.avatar FROM post AS P, profile AS PF WHERE P.id_user IN (" + selections + ") AND P.id_user = PF.id_user ORDER BY date_create DESC";
        try {
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();

            while(rs.next()){
                Article article = new Article();
                article.setIdPost(rs.getInt(1));
                article.setIdUser(rs.getInt(2));
                article.setContent(rs.getString(3));
                article.setDateCreate(rs.getString(4));
                article.setDateUpdate(rs.getString(5));
                article.setNameOfUser(rs.getString(6));
                article.setAvatar(rs.getString(7));
                article.setComments(articleDaoImpl.selectCommentWithidPost(rs.getInt(1),rs.getInt(2)));
                article.setNumberOfLike(articleDaoImpl.selectNumberOfLike(rs.getInt(2), rs.getInt(1)));                
                articles.add(article);
            }
            account.setArticles(articles);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
        
        return account;

	}


    
    public Account insertComment(Account account, String content, int idPost) {
		Comment comment = new Comment();
        String sql = "CALL insert_comment(?,?,?,?,?)";
        Connection cn = DBHelper.returnConnection();
        CallableStatement ps = null;
        try{
            ps = cn.prepareCall(sql);
            ps.setInt(1, account.getIdUser() );
            ps.setString(2, content);
            ps.setInt(3, idPost);
            ps.registerOutParameter(4, Types.INTEGER);
            ps.registerOutParameter(5, Types.DATE);
            ps.executeUpdate();
            comment.setIdComment(ps.getInt(4));
            comment.setContent(content);
            comment.setIdUser(account.getIdUser());
            comment.setIdPost(idPost);
            comment.setNameOfUser(account.getProfile().getName());
            comment.setDateCreate(ps.getString(5));
            comment.setAvatar(account.getProfile().getAvatar());
            for(int i = 0 ; i < account.getArticles().size();i++){
            	if(account.getArticles().get(i).getIdPost() == idPost){
            		 account.getArticles().get(i).getComments().add(comment);
            		 break;
            	}
            }
           
            return account;
        }catch(Exception e){
            return account;
        }finally{
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
	}

	
	public Account selectComment(Account account) {
		String sql = "SELECT id_comment,id_user,id_post,content,date_create,id_parent FROM comment WHERE id_post = ? ORDER BY date_create DESC";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, account.getIdUser());
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				Comment comment = new Comment();
				comment.setIdComment(rs.getInt(1));
				comment.setIdUser(rs.getInt(2));
				comment.setIdPost(rs.getInt(3));
				comment.setContent(rs.getString(4));
				comment.setDateCreate(rs.getString(5));
				//comment.setIdParent(rs.getInt(columnIndex));
				
				for(int i = 0; i < account.getArticles().size();i++){
					if(account.getArticles().get(i).getIdPost() == rs.getInt(3)){
						comment.setAvatar(account.getProfile().getAvatar());
						comment.setNameOfUser(account.getProfile().getName());
						account.getArticles().get(i).getComments().add(comment);
						break;
					}
				}
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}

	
	public List<Comment> selectCommentWithidPost(int idPost,int idUser) {
		String sql = "SELECT P.id_comment,P.id_user,P.id_post,P.content,P.date_create,P.id_parent,PF.avatar,PF.name FROM profile AS PF,comment AS P WHERE P.id_post = ? AND PF.id_user= ? ORDER BY date_create DESC";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<Comment> comments = new ArrayList<Comment>();
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, idPost);
			ps.setInt(2, idUser);
			
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				Comment comment = new Comment();
				comment.setIdComment(rs.getInt(1));
				comment.setIdUser(rs.getInt(2));
				comment.setIdPost(rs.getInt(3));
				comment.setContent(rs.getString(4));
				comment.setDateCreate(rs.getString(5));
				comment.setAvatar(rs.getString(7));
				comment.setNameOfUser(rs.getString(8));
				
				//comment.setIdParent(rs.getInt(columnIndex));
				
				comments.add(comment);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return comments;
	}	
	
	public boolean  insertLike(int id_user, int id_post){
        String sql = "INSERT INTO like_post VALUES ( default,?, ?)";
        Connection cn = DBHelper.returnConnection();
        PreparedStatement ps = null;
        try{
            ps = cn.prepareStatement(sql);
            ps.setInt(1, id_user);
            ps.setInt(2, id_post);
            int result = ps.executeUpdate();
            if( result == 1){
                return true;
            }else {
                
                return false;
            }
        }catch(Exception e){
            System.out.println("Error function like post ");
        }finally{
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
        return false;
    }

    
    public int selectNumberOfLike(int id_user, int id_post) {
        String sql = "SELECT COUNT(id_like) FROM like_post WHERE id_post = ? AND id_user = ?";
        Connection cn = DBHelper.returnConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int result = 0;
        try {
            ps = cn.prepareStatement(sql);
            ps.setInt(1, id_post);
            ps.setInt(2, id_user);
            rs = ps.executeQuery();                       
            
            if(rs.next()){
               result = rs.getInt(1);
                
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
        }
            return result;
    }

    
    public boolean deleteLike(int id_user, int id_post) {
        String sql = "DELETE FROM like_post WHERE id_post = ? AND id_user = ? ";
        Connection cn = DBHelper.returnConnection();
        PreparedStatement ps = null;
        
        try{
            
            ps = cn.prepareStatement(sql);
            ps.setInt(1, id_post);
            ps.setInt(2, id_user);
            
            int result = ps.executeUpdate();
            
            if( result == 1){
                return true;
                
            }else {
                
                return false;
            }
        
        }catch(Exception e){
            System.out.println("Error delete like ");
        
        }finally{
            if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
            if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
            
        }
        
        return false;
    }
}
