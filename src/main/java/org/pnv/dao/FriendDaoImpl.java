package org.pnv.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.pnv.model.Account;
import org.pnv.model.Profile;
import org.pnv.model.RelationShip;
import org.pnv.util.DBHelper;

public class FriendDaoImpl implements FriendDao{

	public boolean addFriend(Account account, String id_friend) {
		String sql ="INSERT INTO relationship(id_user, id_friend,status,date_create) VALUE(?,?,?,now())";
		Connection cn = DBHelper.returnConnection();
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			prepare.setString(2, id_friend );
			prepare.setString(3, "0" );
			prepare.executeUpdate();
			prepare= cn.prepareCall(sql);
			prepare.setString(1, id_friend );
			prepare.setString(2, id_user );
			prepare.setString(3, "2" );
			prepare.executeUpdate();
			return true;
		}catch(Exception e){
			
			System.out.print("error add friend");
			return false;
		}
	}
	
	
	public boolean agreeFriend(Account account, String id_friend) {
		String sql ="UPDATE relationship  SET status = 1, date_update = now() WHERE id_user = ? and id_friend = ?";
		Connection cn = DBHelper.returnConnection();
		
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			prepare.setString(2, id_friend );
			prepare.executeUpdate();
			prepare= cn.prepareCall(sql);
			prepare.setString(1, id_friend );
			prepare.setString(2, id_user );
			prepare.executeUpdate();
			return true;
		}catch(Exception e){
			System.out.print("loi add friend");
			return false;
		}
	}

	
	public boolean deleteFriend(Account account, String id_friend) {
		String sql ="DELETE FROM relationship WHERE id_user = ? and id_friend = ?";
		Connection cn = DBHelper.returnConnection();
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			prepare.setString(2, id_friend );
			prepare.executeUpdate();
			prepare= cn.prepareCall(sql);
			prepare.setString(1, id_friend );
			prepare.setString(2, id_user );
			prepare.executeUpdate();
			return true;
		}catch(Exception e){
			System.out.print("loi add friend");
			return false;
		}
	}
	public Account selectFriendsRequest(Account account) {
		String sql = "SELECT A.id_friend, C.name,C.avatar FROM relationship as A INNER JOIN  account as B INNER JOIN profile as C ON "
				+ "A.id_user = ? and A.id_friend = B.id_user and B.id_user = C.id_user and A.status= ?";
		Connection cn = DBHelper.returnConnection();
		List<Profile> profileList = new ArrayList<Profile>(); 
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id = Integer.toString(account.getIdUser());
			prepare.setString(1, id );
			prepare.setString(2, "2" );
			ResultSet result = prepare.executeQuery();
			while(result.next()){
				 Profile prolile = new Profile();
				 prolile.setId_user(result.getInt(1));
				 prolile.setName(result.getString(2));
				 prolile.setAvatar(result.getString(3));
				 profileList.add(prolile);
			}
			cn.close();
			account.setRequesOfFriend(profileList);
		}catch(Exception e){
			System.out.println( e+"Error function selectFriendsRequest ");
		}
		return account;	
	}
	public Account selectFriendOfUser(Account account) {
		String sql = "SELECT A.id_friend, C.name, C.avatar FROM relationship as A INNER JOIN  account as B INNER JOIN profile as C ON "
				+ "A.id_user = ? and A.id_friend = B.id_user and B.id_user = C.id_user and A.status= 1  and A.id_friend !=?";
		Connection cn = DBHelper.returnConnection();
		List<Profile> profileList = new ArrayList<Profile>(); 
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id = Integer.toString(account.getIdUser());
			prepare.setString(1, id );
			prepare.setString(2, id );
			ResultSet result = prepare.executeQuery();
			while(result.next()){
				 Profile prolile = new Profile();
				 prolile.setId_user(result.getInt(1));
				 prolile.setName(result.getString(2));
				 prolile.setAvatar(result.getString(3));
				 profileList.add(prolile);
			}
			cn.close();
			account.setFriendList(profileList);
		}catch(Exception e){
			System.out.println("Error function sselectFriendOfUser ");
		}
		return account;	
	}
	public Account selectStatusFriend(Account account) {
		String sql = "select id_friend, status from relationship where id_user = ? and id_friend !=?";
		Connection cn = DBHelper.returnConnection();
		List<RelationShip> relationShips = new ArrayList<RelationShip>(); 
		try{
			CallableStatement prepare= cn.prepareCall(sql);
			String id_user = Integer.toString(account.getIdUser());
			prepare.setString(1, id_user );
			prepare.setString(2, id_user );
			ResultSet result = prepare.executeQuery();
			while(result.next()){
				 RelationShip relationShip = new RelationShip();
				 relationShip.setId_friend(result.getInt(1));
				 relationShip.setStatus(result.getInt(2));
				 relationShips.add(relationShip);
			}
			cn.close();
			account.setRelationShip(relationShips);
		}catch(Exception e){
			System.out.println("Error function selectFriend");
		}
		return account;	
	}
	public Account selectPeopleMayKnow(Account account) {
		String sql = "SELECT id_user,name,dob,gender,phone,job,city,avatar FROM profile WHERE MATCH(city) AGAINST(?)";		
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement prepare = null;
		
		try {
			prepare = cn.prepareStatement(sql);
			prepare.setString(1, account.getProfile().getCity());
			rs = prepare.executeQuery();				
			while(rs.next() && account.getPeopleMayKnow().size() < 4){
				if(rs.getInt(1) == account.getIdUser()){
					
				}else{
				Profile profile = new Profile();
				profile.setId_user(rs.getInt(1));
				profile.setName(rs.getString(2));
				profile.setDob(rs.getString(3));
				profile.setGender(rs.getInt(4));
				profile.setPhone(rs.getString(5));
				profile.setJob(rs.getString(6));
				profile.setCity(rs.getString(7));
				profile.setAvatar(rs.getString(8));
				account.getPeopleMayKnow().add(profile);
				}
				
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (prepare != null) try { prepare.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}


}
