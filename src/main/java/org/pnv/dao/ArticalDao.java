package org.pnv.dao;

import java.util.List;

import org.pnv.model.Account;
import org.pnv.model.Comment;

public interface ArticalDao {

	Account selsectArticalOfUser(Account account);
	Account insertArticalOfUser(Account account,String content);
	boolean updateArticalOfUser(int id_user,int id_post,String content);
	boolean deletetArticalOfUser(int id_post,int id_user);
	boolean insertArticalOfUser(int id_user, String content);
	Account selecetArticleWithArticle(Account account);
	
	Account insertComment(Account account, String content, int idPost);
	Account selectComment(Account account);
	List<Comment> selectCommentWithidPost(int idPost, int idUser);
	
	public boolean insertLike(int idUser, int id_post);
    public int selectNumberOfLike(int id_user, int id_post);
    public boolean deleteLike(int idUser, int id_post);
}
