package org.pnv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.pnv.model.Account;
import org.pnv.model.Profile;
import org.pnv.model.SearchResult;
import org.pnv.util.DBHelper;
import org.pnv.util.Utils;

public class ProfileDaoImpl implements ProfileDao{
	
	public Profile selectOneProfile(int id_user) {
		Profile profile = null;
		String sql = "SELECT name,dob,gender,phone,job,city,avatar FROM profile WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, id_user);			
			rs = ps.executeQuery();
			
			if(rs.next()){
				profile = new Profile();
				profile.setName(rs.getString(1));
				profile.setDob(rs.getString(2));
				profile.setGender(rs.getInt(3));				
				profile.setPhone(rs.getString(4));				
				profile.setJob(rs.getString(5));
				profile.setCity(rs.getString(6));				
				profile.setAvatar(rs.getString(7));	
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return profile;
	}
	
	public Account selectProfile(Account account) {
		Profile profile = null;
		String sql = "SELECT name,dob,gender,phone,job,city,avatar FROM profile WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = cn.prepareStatement(sql);
			ps.setInt(1, account.getIdUser());
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				profile = new Profile();
				profile.setName(rs.getString(1));
				profile.setDob(rs.getString(2));
				profile.setGender(rs.getInt(3));
				profile.setPhone(rs.getString(4));
				profile.setJob(rs.getString(5));
				profile.setCity(rs.getString(6));
				profile.setAvatar(rs.getString(7));
				account.setProfile(profile);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return account;
	}

	
	public boolean isUpdateInformationSuccessfull(Account account) {
		
		String sql = "UPDATE profile SET name = ?, dob = ?, gender = ?, phone = ?, job = ?, city = ? WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		try {
			PreparedStatement prepare = cn.prepareStatement(sql);
			prepare.setString(1, account.getProfile().getName() );
			prepare.setString(2, account.getProfile().getDob());
			prepare.setInt(3, account.getProfile().getGender());
			prepare.setString(4, account.getProfile().getPhone() );
			prepare.setString(5, account.getProfile().getJob());
			prepare.setString(6, account.getProfile().getCity());
			prepare.setInt(7,account.getIdUser());
			
			int result = prepare.executeUpdate();
			
			if(result == -1){
				cn.close();
				return false;
			}else{
				cn.close();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	public boolean isUpdateInformationSuccessfull(Account account, String name, int gender, String dob, String job, String city, String phone) {
		
		String sql = "UPDATE profile SET name = ?, dob = ?, gender = ?, phone = ?, job = ?, city = ? WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		try {
			PreparedStatement prepare = cn.prepareStatement(sql);
			prepare.setString(1, name );
			prepare.setString(2, dob);
			prepare.setInt(3,gender);
			prepare.setString(4, phone);
			prepare.setString(5, job);
			prepare.setString(6, city);
			prepare.setInt(7,account.getIdUser());
			
			int result = prepare.executeUpdate();
			
			if(result == -1){
				cn.close();
				return false;
			}else{
				cn.close();
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}

	
	public boolean isUpdateAvatarSuccessful(String linkAvatar, int id) {
		String sql = "UPDATE profile SET avatar = ? WHERE id_user = ?";
		Connection cn = DBHelper.returnConnection();
		try {
			PreparedStatement prepare = cn.prepareStatement(sql);
			prepare.setString(1, linkAvatar);
			prepare.setInt(2, id);
			
			int result = prepare.executeUpdate();
			if( result == 1 ){
				cn.close();
				return true;
				
			}else{
				cn.close();
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}

	
	public String selectAvatar(int id) {
		String sql = "SELECT avatar FROM profile WHERE id_user = ?";

		Connection cn = DBHelper.returnConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = null;
		try {
			ps = cn.prepareStatement(sql);

			ps.setInt(1, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (ps != null) try { ps.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		
		return result;
	}
	

	
	public boolean isUpdatePasswordSuccessful(String email, String oldPassword, String newPassword) {
		String sql = "UPDATE account SET password = ? WHERE email = ? AND password = ?";
		Connection cn = DBHelper.returnConnection();
		try {
			PreparedStatement prepare = cn.prepareStatement(sql);
			prepare.setString(1, Utils.convertPasswordToMD5(newPassword));
			prepare.setString(2, email);
			prepare.setString(3, Utils.convertPasswordToMD5(oldPassword));
			
			int check = prepare.executeUpdate();
			
			if(check == 1){
				cn.close();
				return true;
			}else{
				cn.close();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public List<SearchResult> search(String nameUserSearch) {
		String sql = "SELECT id_user FROM profile WHERE name LIKE '%"+nameUserSearch +"%' ";		
		Connection cn = DBHelper.returnConnection();
		ResultSet rs = null;
		PreparedStatement prepare = null;
		try {
			List<SearchResult> result =  new ArrayList<SearchResult>();
			prepare = cn.prepareStatement(sql);
			rs = prepare.executeQuery();				
			while(rs.next()){
				SearchResult res = new SearchResult();
				int id_user= Integer.parseInt(rs.getString(1));
				Profile profile = selectOneProfile(id_user);
				res.setId(id_user);
				res.setProfile(profile);
				result.add(res);
			}
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
	        if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
	        if (prepare != null) try { prepare.close(); } catch (SQLException ignore) {}
	        if (cn != null) try { cn.close(); } catch (SQLException ignore) {}
	    }
		return null;
	}
	
}
